from pathlib import Path
import os

# ensure relative path for script is correct
try:
    if os.environ['WINDIR'] == 'C:\\WINDOWS':
        os.chdir(Path('D:\\','code','repos','adventofcode2020'))
        print(f'On Dao\'s desktop.\nPath is {Path.cwd()}.')
except Exception as exc:
    print(f'Not on Dao\'s desktop. Setting path to Macbook directory:')
    os.chdir(Path('/Users', 'nintendao', 'code', 'repos', 'adventofcode2020'))
    print(f'Path is {Path.cwd()}.')

# read input file, acquire as a list
mapFile = Path('dayThree_input.txt')
mapFileList = open(mapFile).readlines()
# print(mapFileList)
# initialise counters
numTrees = 0    # number of trees, illustrated as '#' character
row = 0         # row counter
col = 0         # column counter

# want to get to the bottom of the 'slope'
while row+1 < len(mapFileList):
    row += 1
    step = 0
    while step < 3:
        # move one column right at a time, and check if column position is the end
        # column position goes back to 0 once it has reached the end in the same step loop
        col += 1
        if col == len(mapFileList[row])-1:      #-1 on the column length to accommodate for zero-indexing
            col = 0
        step += 1
    if mapFileList[row][col] == '#':
        numTrees += 1

print(f'Number of trees that you would run into is {numTrees}.')
open(mapFile).close()
