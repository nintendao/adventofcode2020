from pathlib import Path
import os

#ensure relative path for script is correct#############################################################################
try:
    if os.environ['WINDIR'] == 'C:\\WINDOWS':
        os.chdir(Path('D:\\','code','repos','adventofcode2020'))
        print(f'On Dao\'s desktop.\nPath is {Path.cwd()}.')
except Exception as exc:
    print(f'Not on Dao\'s desktop. Setting path to Macbook directory:')
    os.chdir(Path('/Users', 'nintendao', 'code', 'repos', 'adventofcode2020'))
    print(f'Path is {Path.cwd()}.')
########################################################################################################################

mapFile = Path('dayThree_input.txt')
mapFileList = open(mapFile).readlines()
# print(mapFileList)

def tobogganTrajectory(numrowsteps, numcolsteps):
    # initialise counters
    row = 0
    col = 0
    numtrees = 0
    # want to get to the bottom of the 'slope'
    while row+1 < len(mapFileList):                 # +1 so that you don't step beyond the last row
        row += numrowsteps
        step = 0
        # when the end of the column is reached, continue the steps for that loop from column zero
        while step < numcolsteps:
            col +=1
            if col == len(mapFileList[row])-1:      # -1 to offset zero-indexing
                col = 0
            step += 1
        if mapFileList[row][col] == '#':
            numtrees +=1

    open(mapFile).close()
    return numtrees

slopesList = [[1,1],[1,3],[1,5],[1,7],[2,1]]
treeList = []
product = 1
for args in slopesList:
    treeList.append(tobogganTrajectory(args[0],args[1]))

for numTrees in treeList:
    product = product * numTrees

print(f'the product trees encountered across all slopes is {product}.')



