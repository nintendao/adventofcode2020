'''
Suppose you have the following list:

1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc

Each line gives the password policy and then the password. The password policy indicates the lowest and highest
number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password
must contain a at least 1 time and at most 3 times.

In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no instances of b,
but needs at least 1. The first and third passwords are valid: they contain one a or nine c, both within the limits
of their respective policies.

Find the number of valid passwords according to their password policies.
'''
from pathlib import Path
import os, re
# ensure relative path for script is correct
try:
    if os.environ['WINDIR'] == 'C:\\WINDOWS':
        os.chdir(Path('D:\\','code','repos','adventofcode2020'))
        print(f'On Dao\'s desktop.\nPath is {Path.cwd()}.')
except Exception as exc:
    print(f'Not on Dao\'s desktop. Setting path to Macbook directory:')
    os.chdir(Path('/Users', 'nintendao', 'code', 'repos', 'adventofcode2020'))
    print(f'Path is {Path.cwd()}.')

#read input file
passwordsFile = Path('dayTwo_input.txt')
passwordsList = open(passwordsFile).readlines()

# initialise variables
passwordsDict = {'policy':
    {
        'min': 0,
        'max': 0
    },
    'letter': 'char',
    'password': 'char'
}
validPasswords = 0
invalidPasswords = 0
passwordsCounterList = []
for row in passwordsList:
    # regex to parse the row
    passwordRegex = re.compile(r'(\d+)-(\d+) ([a-zA-Z]): (\w+)')
    '''
    rows look like so:
    1-3 a: abcde
    1-3 b: cdefg
    2-9 c: ccccccccc
    '''
    # add keys/properties to passwordsDict according to the initial structure
    passwordsDict = {}
    findValues = passwordRegex.search(row)
    passwordsDict['policy'] = {}
    passwordsDict['policy']['min'] = int(findValues.group(1))
    passwordsDict['policy']['max'] = int(findValues.group(2))
    passwordsDict['letter'] = findValues.group(3)
    passwordsDict['password'] = findValues.group(4)

    # append the refactored password to the list of passwordDicts
    passwordsCounterList.append(passwordsDict)
    # print(passwordsCounterList)

# Check the validity of the password against the policy
for entry in passwordsCounterList:
    #counter to track number of times the policy is met- cannot be less than the min, and more than the max.
    policyCounter = 0
    policyMin = entry['policy']['min']
    policyMax = entry['policy']['max']
    letter = entry['letter']
    password = entry['password']

    for char in password:
        if char == letter:
            policyCounter = policyCounter + 1
    if policyCounter < policyMin or policyCounter > policyMax:
        invalidPasswords = invalidPasswords + 1
    else:
        validPasswords = validPasswords + 1

print(f'number of valid passwords is {validPasswords}.')
print(f'number of invalid passwords is {invalidPasswords}.')


