'''
Provide 1319441-20201219-a2059fab if you are asked to prove you own this account by an Advent of Code administrator.
Don't post this code in a public place.
'''
'''
Find the two numbers in the list that add to 2020. The answer is the product of those two numbers.
'''

from pathlib import Path
import os

try:
    if os.environ['WINDIR'] == 'C:\\WINDOWS':
        os.chdir(Path('D:\\','code','repos','adventofcode2020'))
        print(f'On Dao\'s desktop.\nPath is {Path.cwd()}.')
except Exception as exc:
    print(f'Not on Dao\'s desktop. Setting path to Macbook directory:')
    os.chdir(Path('/Users', 'nintendao', 'code', 'repos', 'adventofcode2020'))
    print(f'Path is {Path.cwd()}.')

expenseReport = Path('dayOne_input.txt')
readExpenseReport = open(expenseReport).readlines()

numbersList = []
for entry in readExpenseReport:
    numbersList.append(int(entry))
#counters to track the index of a and b being added
counterA = 0
counterB = 0
answer = 0
for a in numbersList:
    if answer != 0:
        break
    for b in numbersList:
        #only add a and b if they are not referencing the same index
        if counterA != counterB:
            if a + b == 2020:
                answer = a * b
                print(f'a is {a}, b is {b}\na * b is {answer}')
                break
        counterB = counterB + 1
    counterA = counterA + 1
    counterB = 0






