from pathlib import Path
import os

#ensure relative path for script is correct#############################################################################
try:
    if os.environ['WINDIR'] == 'C:\\WINDOWS':
        os.chdir(Path('D:\\','code','repos','adventofcode2020'))
        print(f'On Dao\'s desktop.\nPath is {Path.cwd()}.')
except Exception as exc:
    print(f'Not on Dao\'s desktop. Setting path to Macbook directory:')
    os.chdir(Path('/Users', 'nintendao', 'code', 'repos', 'adventofcode2020'))
    print(f'Path is {Path.cwd()}.')
########################################################################################################################

with open('dayFour_input.txt') as inputFile:
    # receive contents of the file as a single string. Common separator between passports is a new line char.
    # Data = inputFile.read()
    Data = inputFile.readlines()
    print(Data)
    passportBatch = []
    value = ''
    for row in Data:
        # Newline rows separate passports. In that case, add the passport to the passportBatch
        if row == '\n':
            passportBatch.append(value)
            value = ''
        else:
            value += row
    print(passportBatch)

    def passportPropChecker(propid, passportitem):
        if propid in passportitem:
            propChecklist[propid] = True
        return
    passportProps = ["ecl", "pid", "eyr", "hcl", "byr", "iyr", "cid", "hgt"]
    numValidPassports = 0
    # check for presence of properties
    for passport in passportBatch:
        validPassport = True
        validPassportCounter = 0
        propChecklist = {
            "ecl": False,
            "pid": False,
            "eyr": False,
            "hcl": False,
            "byr": False,
            "iyr": False,
            "cid": False,
            "hgt": False
        }
        for prop in passportProps:
            passportPropChecker(prop, passport)

        if propChecklist['ecl'] is False:
            validPassport = False
        if propChecklist['eyr'] is False:
            validPassport = False
        if propChecklist['hcl'] is False:
            validPassport = False
        if propChecklist['byr'] is False:
            validPassport = False
        if propChecklist['iyr'] is False:
            validPassport = False
        if propChecklist['pid'] is False:
            validPassport = False
        if propChecklist['hgt'] is False:
            validPassport = False
        if validPassport is True:
            numValidPassports += 1
    print(numValidPassports)

