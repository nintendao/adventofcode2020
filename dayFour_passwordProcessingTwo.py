from pathlib import Path
import os, re

# ensure relative path for script is correct############################################################################
try:
    if os.environ['WINDIR'] == 'C:\\WINDOWS':
        os.chdir(Path('D:\\','code','repos','adventofcode2020'))
        print(f'On Dao\'s desktop.\nPath is {Path.cwd()}.')
except Exception as exc:
    print(f'Not on Dao\'s desktop. Setting path to Macbook directory:')
    os.chdir(Path('/Users', 'nintendao', 'code', 'repos', 'adventofcode2020'))
    print(f'Path is {Path.cwd()}.')
########################################################################################################################
numValidPassports = 0                       # counter for the number of valid passports found
numInvalidPassports = 0
passportFields = ['iyr', 'pid', 'ecl', 'hcl', 'hgt', 'eyr', 'byr']

# function to check that a field exists in a passport - returns 1 if that field exists, 0 if it doesn't
def passportfieldchecker(field, passport):
    if field not in passport:
        return 0
    else:
        return 1

# function to check validity of the data in a passport - returns True for valid, False for invalid
def passportdatachecker(passport):
    passportfieldsvalues = re.compile(r'([a-zA-Z]{3}):([^ ]{1,})').findall(passport)
    #print(passportfieldsvalues)
    passportkv = {}
    for i in passportfieldsvalues:
        passportkv[i[0]] = i[1]

    if re.compile(r'^201\d$|^2020$').search(passportkv['iyr']) is None:                                             # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        return False
    if re.compile(r'^202\d$|^2030$').search(passportkv['eyr']) is None:                                             # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        return False
    if re.compile(r'^#[\da-f]{6}$').search(passportkv['hcl']) is None:                                              # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        return False
    if re.compile(r'^amb$|^blu$|^brn$|^gry$|^grn$|^hzl$|^oth$').search(passportkv['ecl']) is None:                  # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        return False
    if re.compile(r'^19[23456789]\d$|^200[012]$').search(passportkv['byr']) is None:                                # byr (Birth Year) - four digits; at least 1920 and at most 2002.
        return False
    if re.compile(r'^59in$|^6\din$|^7[0123456]in$|^1[5678]\dcm$|^19[0123]cm$').search(passportkv['hgt']) is None:   # hgt (Height) - a number followed by either cm or in:
        return False                                                                                                # If cm, the number must be at least 150 and at most 193
                                                                                                                    # If in, the number must be at least 59 and at most 76.
    if re.compile(r'^\d{9}$').search(passportkv['pid']) is None:                                                    # pid (Passport ID) - a nine-digit number, including leading zeroes.
        return False

    return True

with open('dayFour_input.txt') as inputFile:
    # receive contents of the file as a single string. Common separator between passports is a newline char.
    Data = inputFile.readlines()
    # print(Data)
    passportBatch = []                  # List of all passports
    value = ''                          # passports are sorted into this variable to be added to the passport list
    for row in Data:
        if row == '\n':                 # newline rows separate passports
            passportBatch.append(value)
            value = ''
        else:
            value += row

    # print(passportBatch)

    # clean passport list - remove ending space characters, and newlines are replaced with a space
    passportBatchClean = [re.compile(r'[ ]$').sub('', passport) for passport in [
        re.compile(r'\n').sub(' ', passport) for passport in passportBatch
    ]]
    # filter the passport batch to only include those with the necessary fields
    passportBatchCleanAllFields = []     # List of passports with necessary fields
    for passport in passportBatchClean:
        numFields = 0
        for field in passportFields:
            numFields += passportfieldchecker(field, passport)
        if numFields == 7:
            passportBatchCleanAllFields.append(passport)

    # finally, count the number of valid passports
    for i in passportBatchCleanAllFields:
        if passportdatachecker(i) is True:
            numValidPassports += 1
        else:
            numInvalidPassports += 1


print(f'number of valid passports is {numValidPassports}.')
print(f'number of invalid passports is {numInvalidPassports}.')
