'''
Find the three numbers in the list that add to 2020. The answer is the product of those three numbers.
'''

from pathlib import Path
import os

try:
    if os.environ['WINDIR'] == 'C:\\WINDOWS':
        os.chdir(Path('D:\\','code','repos','adventofcode2020'))
        print(f'On Dao\'s desktop.\nPath is {Path.cwd()}.')
except Exception as exc:
    print(f'Not on Dao\'s desktop. Setting path to Macbook directory:')
    os.chdir(Path('/Users', 'nintendao', 'code', 'repos', 'adventofcode2020'))
    print(f'Path is {Path.cwd()}.')

expenseReport = Path('dayOne_input.txt')
readExpenseReport = open(expenseReport).readlines()

numbersList = []
for entry in readExpenseReport:
    numbersList.append(int(entry))

#the initial index trackers for the three numbers: a, b, and c
counterA = 0
counterB = 0
counterC = 0

answer = 0
for a in numbersList:
    if answer != 0:
        break
    for b in numbersList:
        # only add A and B if they aren't referencing the same index
        if answer != 0:
            break
        if counterA != counterB:
            if a + b < 2020:
                for c in numbersList:
                    # only add C if it isn't referencing the same index of either A or B
                    if counterC != counterA or counterC != counterB:
                        if a + b + c == 2020:
                            answer = a * b * c
                            print(f'a is {a}, b is {b}, c is {c}.\na * b is {answer}.')
                            break
                    counterC = counterC + 1
        counterB = counterB + 1
        counterC = 0
    counterA = counterA + 1
    counterB = 0
