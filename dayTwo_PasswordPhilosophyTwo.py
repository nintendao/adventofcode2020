'''
While it appears you validated the passwords correctly, they don't seem to be what the Official Toboggan Corporate
Authentication System is expecting.

The shopkeeper suddenly realizes that he just accidentally explained the password policy rules from his old job at
the sled rental place down the street! The Official Toboggan Corporate Policy actually works a little differently.

Each policy actually describes two positions in the password, where 1 means the first character, 2 means the second
character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) Exactly one of these
positions must contain the given letter. Other occurrences of the letter are irrelevant for the purposes of policy
enforcement.

Given the same example list from above:

    1-3 a: abcde is valid: position 1 contains a and position 3 does not.
    1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
    2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.

'''
from pathlib import Path
import os, re

# ensure relative path for script is correct
try:
    if os.environ['WINDIR'] == 'C:\\WINDOWS':
        os.chdir(Path('D:\\','code','repos','adventofcode2020'))
        print(f'On Dao\'s desktop.\nPath is {Path.cwd()}.')
except Exception as exc:
    print(f'Not on Dao\'s desktop. Setting path to Macbook directory:')
    os.chdir(Path('/Users', 'nintendao', 'code', 'repos', 'adventofcode2020'))
    print(f'Path is {Path.cwd()}.')

#read input file
passwordsFile = Path('dayTwo_input.txt')
passwordsList = open(passwordsFile).readlines()

# initialise variables
passwordsDict = {'policy':
    {
        'inIndex': 0,
        'notInIndex': 0
    },
    'letter': 'char',
    'password': 'char'
}
validPasswords = 0
invalidPasswords = 0
passwordsCounterList = []
for row in passwordsList:
    # regex to parse the row
    passwordRegex = re.compile(r'(\d+)-(\d+) ([a-zA-Z]): (\w+)')
    '''
    rows look like so:
    1-3 a: abcde
    1-3 b: cdefg
    2-9 c: ccccccccc
    '''
    # add keys/properties to passwordsDict according to the initial structure
    passwordsDict = {}
    findValues = passwordRegex.search(row)
    passwordsDict['policy'] = {}
    passwordsDict['policy']['indexFir'] = (int(findValues.group(1))-1) # -1 to adjust for the lack of 'index zero'
    passwordsDict['policy']['indexSec'] = (int(findValues.group(2))-1) # -1 to adjust for the lack of 'index zero'
    passwordsDict['letter'] = findValues.group(3)
    passwordsDict['password'] = findValues.group(4)

    # append the refactored password to the list of passwordDicts
    passwordsCounterList.append(passwordsDict)
    # print(passwordsCounterList)

# Check the validity of the password against the policy
for entry in passwordsCounterList:
    # counter to track number of times the policy is met-
        # Exactly one of these positions must contain the given letter.
    policyCounter = 0
    policyIndexFir = entry['policy']['indexFir']
    policyIndexSec = entry['policy']['indexSec']
    letter = entry['letter']
    password = entry['password']
    if password[policyIndexFir] == letter:
        if password[policyIndexSec] != letter:
            validPasswords = validPasswords + 1
        else:
            invalidPasswords = invalidPasswords + 1
    else:
        if password[policyIndexFir] != letter:
            if password[policyIndexSec] == letter:
                validPasswords = validPasswords + 1
            else:
                invalidPasswords = invalidPasswords + 1

print(f'number of valid passwords is {validPasswords}.')
print(f'number of invalid passwords is {invalidPasswords}.')


